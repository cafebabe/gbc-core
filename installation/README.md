# Packages of GameBox Cloud Core

-   Redis
    -   redis-2.6.16.tar.gz

-   beanstalkd
    -   beanstalkd-1.9.tar.gz

-   openresty
    -   ngx\_openresty-1.7.7.1.tar.gz

-   luahttpclient
    -   luahttpclient.tar.gz

-   luainspect
    -   luainspect.tar.gz

-   luasec
    -   luasec-0.5.tar.gz

-   luasocket
    -   luasocket-3.0-rc1.tar.gz
