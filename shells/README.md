# some tools for GameBox Cloud Core.

-   start\_server
    -   start nginx process.
    -   start redis sever.
    -   start beanstalkd process.
    -   start job worker process.
    -   start monitor process.

-   stop\_server
    -   stop nginx process.
    -   stop redis process.
    -   stop beanstalkd process.
    -   stop job worker process.
    -   stop monitor process.
    -   reload ngxin conf file and restart nginx only(with --reload option).

-   check\_server
    -   show nginx process.
    -   show redis process.
    -   show beanstalkd process.
    -   show job worker process.
    -   show monitor process.

-   tools.sh
    -   start a server tool written with GameBox Cloud Core.
